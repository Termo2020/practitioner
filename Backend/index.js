//Se incluye el módulo con el cual interactúo con Mongo
require('dotenv').config();
const cors = require('cors');

const mongoose = require('mongoose')
const express = require('express')
const app = express()

//Incluye las rutas en donde se ejecutan las peticiones
const user = require('./routes/user')
const auth = require('./routes/auth')
const cuenta = require('./routes/cuenta')
const movimientos = require('./routes/movimientos')

app.use(cors());
app.use(express.json());
app.use(user);
app.use(auth);
app.use(cuenta);
app.use(movimientos);

app.use(express.json());
app.use('/api/user/', user);
app.use('/api/auth/', auth);
app.use('/api/cuenta/', cuenta);
app.use('/api/movimientos', movimientos);
//Si no tiene asignado un puerto usa el 3003
const port = process.env.PORT || 3003
app.listen(port, ()=> console.log('Escuchando Puerto: ' + port))

//Si no existe la base de datos, la crea
//URI de MongoDB
process.env.MONGO_URI = process.env.MONGO_URI || "localhost:27017";
console.log(process.env.MONGO_URI)
    mongoose.connect(`mongodb://${ process.env.MONGO_URI }/bankDB`, 
    //mongoose.connect('mongodb://localhost:27017/bankDB', 
    {
        useNewUrlParser:true, 
        useUnifiedTopology: true, 
        useFindAndModify: false,
        useCreateIndex: true
    })
    .then(()=> console.log('Conectado a MongoDb'))
    .catch(erro=> console.log('No se ha conectado a MongoDb'))