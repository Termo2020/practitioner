const mongoose = require('mongoose')
const cuentaSchema = new mongoose.Schema({
    DNI:{
        type: Number        
    },
    tipo: {
        type: String
    },
    cuenta: {
        type: Number
    },
    saldo: {
        type: Number
    },
    date: {type: Date, default: Date.now}
})

const Cuenta = mongoose.model('cuenta', cuentaSchema)

module.exports = Cuenta