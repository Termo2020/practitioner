const mongoose = require('mongoose')
const movimientosSchema = new mongoose.Schema({
    DNI:{type: Number},
    tipo: {type: String},
    cuenta: {type: Number},
    movimiento:{type: String},
    importe: {type: Number},
    date: {type: Date, default: Date.now}
})

const Movimientos = mongoose.model('movimientos', movimientosSchema)

module.exports = Movimientos