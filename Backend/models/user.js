require('dotenv').config();
const mongoose = require('mongoose')
const jwt = require('jsonwebtoken')

const userSchema = new mongoose.Schema({
    nombre:{
        type: String,
        required: true,
        maxlength: 50
    },
    apellido: {
        type: String,
        required: true,
        maxlength: 50
    },
    DNI: {
        type: Number,
        required: true,
        maxlength: 8
    },
    email: {
        type: String,
        maxlength: 70,
        required: true
    },
    clave: {
        type: String,
        required: true
    },
    date: {type: Date, default: Date.now}
})

//Genera el token y devuelve id, nombre, apellido
userSchema.methods.generateJWT = function(){
    return jwt.sign({
        _id: this._id, 
        nombre: this.nombre,
        apellido: this.apellido
        //isAdmin: this.isAdmin,
        //role: this.role
    }, process.env.SECRET_KEY_JWT_CAR_API)
}
 
const User = mongoose.model('user', userSchema)
module.exports = User