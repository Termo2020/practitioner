const bcrypt = require('bcrypt')  
const mongoose = require('mongoose')
const express = require('express')
const User = require('../models/user')
const auth = require('../middleware/auth')
const router = express.Router()
const { check, validationResult } = require('express-validator');
router.post('/', [ 
    //Se realizan las validaciones
    check('email').isLength({min: 3}),
    check('clave').isLength({min:3})
], async(req,res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() });
    }

    let user = await User.findOne({email: req.body.email})
    if(!user) return res.status(400).send('Usuario o contraseña incorrectos')
    //Compara y devuelve un boolean
    const validPassword = await bcrypt.compare(req.body.clave, user.clave)
    if(!validPassword) return res.status(400).send('Usuario o contraseña incorrectos')

    const jwtToken = user.generateJWT();
    //res.status(201).header('authorization', jwtToken).send({
    res.status(201).send({
        //_id: user._id,
        nombre: user.nombre,
        DNI   : user.DNI,
        email : user.email,
        token : jwtToken,
    })


})

module.exports = router