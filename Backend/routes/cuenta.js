//Modulo para conectar con Mongodb
const mongoose = require('mongoose')
const express = require('express')
const Cuentas = require('../models/cuentas')
const auth   = require('../middleware/auth')
const router = express.Router()

router.get('/api/cuenta/:DNI/:tipo', [auth], async(req, res)=>{
    const session = await mongoose.startSession()//
    session.startTransaction()    
    try{        
        const cuentaConsulta = await Cuentas.find({DNI: req.params.DNI, tipo: req.params.tipo})
        if(!cuentaConsulta) return res.status(404).send('No hemos encontrado cuenta para ese usuario')
        res.send(cuentaConsulta)
    }catch(e){
        await session.abortTransaction()
        session.endSession
        res.status(500).send(e.message)
    }

})

////////////////////////////////////////////
// Modificación de saldos de las cuentas //
///////////////////////////////////////////
router.put('/api/cuenta/:_id', [auth], async(req,res) => {
    const session = await mongoose.startSession()//
    session.startTransaction()        
    try{          
        const cuentaOrigen = await Cuentas.findById({_id: req.params._id})        
        if(!cuentaOrigen) return res.status(404).send('No hemos encontrado cuenta para ese usuario')
        saldoCalculadoOrigen = parseFloat(cuentaOrigen.saldo) - parseFloat(req.body.importeOrigen) 
                               
        const cuentaSaldoOrigen = await Cuentas.findByIdAndUpdate({_id: req.params._id}, {
            saldo: saldoCalculadoOrigen.toFixed(2)
        },
        {
            new: true
        }) 
        const cuentaDestino = await Cuentas.find({DNI: req.body.DNI, tipo: req.body.tipoDestino})
        saldoCalculadoDestino = parseFloat(cuentaDestino[0].saldo) + parseFloat(req.body.importeDestino)
              
        const cuentaSaldoDestino = await Cuentas.findByIdAndUpdate({_id: cuentaDestino[0]._id}, {
            saldo: saldoCalculadoDestino.toFixed(2)
        },
        {
            new: true
        })
        const resultado = {}
        resultado.saldo = saldoCalculadoOrigen.toFixed(2)
        res.status(201).send(resultado)
    }catch(e){
        await session.abortTransaction()
        session.endSession
        res.status(500).send(e.message)
    }
})


module.exports = router