const mongoose = require('mongoose')
const express = require('express')
const Movimientos = require('../models/movimientos')
const Cuentas = require('../models/cuentas')
const auth = require('../middleware/auth')
const router = express.Router()
const { check, validationResult } = require('express-validator');

////////////////////////
//Alta de movimientos //
///////////////////////
router.post('/',  [
    //Validación express-validator
    check('DNI').not().isEmpty().isLength({max: 8}),
    check('tipo').not().isEmpty(),    
    check('cuenta').not().isEmpty(),    
    check('importe').not().isEmpty()      
    ], [auth], async(req,res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() });
    }
    let importeCalculado, importeOriginal = 0;
    let identificador ="";
    //Accedo al API de cotización
    let cuentaConsulta = await Cuentas.find({DNI: req.body.DNI, tipo: req.body.tipo});
    if (cuentaConsulta){
        identificador = cuentaConsulta[0]._id;        
    }
    if (req.body.tipo == "USD"){
        importeCalculado = parseFloat(req.body.importe) * parseFloat(req.body.cotizacion);
        importeOriginal = parseFloat(req.body.importe);
        
        let movimientosOrigen = new Movimientos({
                DNI        : req.body.DNI,
                tipo       : req.body.tipo,
                cuenta     : req.body.cuenta,
                movimiento : "Débito",
                importe    : importeOriginal.toFixed(2)
            })
        let resultOrigen = await movimientosOrigen.save()

        cuentaConsulta = await Cuentas.find({DNI: req.body.DNI, tipo: "ARS"})
        let movimientosDestino = new Movimientos({
            DNI        : req.body.DNI,
            tipo       : "ARS",
            cuenta     : cuentaConsulta[0].cuenta,
            movimiento : "Crédito",
            importe    : importeCalculado.toFixed(2)
        })
        let resultDestino = await movimientosDestino.save()
        const resultado = {};
        resultado.id             = identificador;    
        resultado.DNI            = movimientosOrigen.DNI;
        resultado.tipoOrigen     = movimientosOrigen.tipo;
        resultado.cuentaOrigen   = movimientosOrigen.cuenta;
        resultado.importeOrigen  = movimientosOrigen.importe;
        resultado.tipoDestino    = movimientosDestino.tipo;
        resultado.cuentaDestino  = movimientosDestino.cuenta;
        resultado.importeDestino = movimientosDestino.importe;

        res.status(201).send(resultado)
    }else if (req.body.tipo == "ARS"){
        importeCalculado = parseFloat(req.body.importe) / parseFloat(req.body.cotizacion); 
        importeOriginal = parseFloat(req.body.importe);  
        let movimientosOrigen = new Movimientos({
                DNI        : req.body.DNI,
                tipo       : req.body.tipo,
                cuenta     : req.body.cuenta,
                movimiento : "Débito",
                importe    : importeOriginal.toFixed(2)
            })
            
        let resultOrigen = await movimientosOrigen.save()
        let movimientosDestino = new Movimientos({
            DNI        : req.body.DNI,
            tipo       : "USD",
            cuenta     : req.body.cuenta,
            movimiento : "Crédito",
            importe    : importeCalculado.toFixed(2)
        })
        let resultDestino = await movimientosDestino.save()
        const resultado = {};
        resultado.id             = identificador;
        resultado.DNI            = movimientosOrigen.DNI;
        resultado.tipoOrigen     = movimientosOrigen.tipo;
        resultado.cuentaOrigen   = movimientosOrigen.cuenta;
        resultado.importeOrigen  = movimientosOrigen.importe;
        resultado.tipoDestino    = movimientosDestino.tipo;
        resultado.cuentaDestino  = movimientosDestino.cuenta;
        resultado.importeDestino = movimientosDestino.importe;

        res.status(201).send(resultado)        
    }
})

//////////////////////////////////////
// Consulta de movimientos por DNI //
////////////////////////////////////
router.get('/:DNI', [auth], async(req, res)=>{
    const session = await mongoose.startSession()
    session.startTransaction()
    try{
        const movimientos = await Movimientos.find({DNI: req.params.DNI}).sort('-date').limit(6)
        if(!movimientos) return res.status(404).send('No hemos encontrado movimientos para ese cliente')
        res.send(movimientos)        
    }catch(e){
        await session.abortTransaction()
        session.endSession
        res.status(500).send(e.message)
    }
})
module.exports = router
