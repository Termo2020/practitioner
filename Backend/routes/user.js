//Modulo para encriptar
const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')
//Modulo para conectar con Mongodb
const mongoose = require('mongoose')
const express = require('express')
const User = require('../models/user')
const Cuenta = require('../models/cuentas')

const auth = require('../middleware/auth')

const router = express.Router()
//Modulo para validar los datos recibidos
const { check, validationResult } = require('express-validator');

//Consulta de todos los usuario
router.get('/', [auth], async(req, res)=>{
    const session = await mongoose.startSession()//
    session.startTransaction()//
    try{
        const user = await User.find()
        res.send(user)
    }catch(e){
        await session.abortTransaction()
        session.endSession
        res.status(500).send(e.message)
    }
})

//Busca un usuario puntual
router.get('/:email', [auth], async(req, res)=>{
    const session = await mongoose.startSession()//
    session.startTransaction()//
    try{
        const user = await User.findOne({email: req.params.email})
        if(!user) return res.status(404).send('No hemos encontrado un usuario con ese email')
        console.log(user)
        res.send(user)
    }catch(e){
        await session.abortTransaction()
        session.endSession
        res.status(500).send(e.message)
    }
})

router.post('/', [
    //Validación express-validator
    check('nombre').not().isEmpty().isLength({max: 50}),    
    check('apellido').not().isEmpty().isLength({max: 50}),
    check('DNI').not().isEmpty().isLength({max: 8}),
    check('email').not().isEmpty().isEmail().isLength({max: 70}),    
    check('clave').not().isEmpty().isLength({min: 6}).isLength({max: 15})    
], async(req,res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() });
    }
    //Verifica si existe el email en la colección y muestra un error en tal caso
    let user = await User.findOne({email: req.body.email})
    if(user) return res.status(400).send('Ese usuario ya existe')
    //Encripta la clave del usuario para guardarla
    const salt = await bcrypt.genSalt(10)
    const hashPassword = await bcrypt.hash(req.body.clave, salt)
    //Crea documento nuevo con los datos recibidos
    user = new User({ 
        nombre  : req.body.nombre,
        apellido: req.body.apellido,
        DNI     : req.body.DNI,
        email   : req.body.email,
        clave   : hashPassword
    })
    //Guarda los datos de usuario
    const result = await user.save()
    //Se busca la última cuenta ARS para incrementar en 1 e insertar la cuenta 
    let ultimoARS = await Cuenta.findOne({tipo:"ARS"},{cuenta:1}).limit(1).sort({$natural:-1})
    if(!ultimoARS)
    {
        cuentaARS = 1111111
    }
    else
    {
        cuentaARS = Number(ultimoARS.cuenta) + 1
       
    }
    //Se busca la última cuenta USD para incrementar en 1 e insertar la cuenta 
    let ultimoUSD = await Cuenta.findOne({tipo:"USD"},{cuenta:1}).limit(1).sort({$natural:-1})
    if(!ultimoUSD)
    {
        cuentaUSD = 5555555
    }
    else
    {
        cuentaUSD = Number(ultimoUSD.cuenta) + 1
       
    }
    //Graba cuenta ARS
    cuenta = new Cuenta({
        DNI   : req.body.DNI,
        tipo  : "ARS",
        cuenta: cuentaARS,
        saldo : 10000.00
    })
    const resultARS = await cuenta.save()
    //Graba cuenta USD
    cuenta = new Cuenta({
        DNI   : req.body.DNI,
        tipo  : "USD",
        cuenta: cuentaUSD,
        saldo : 13000.00
    })
    const resultUSD = await cuenta.save()
    //Emite mensaje informando que es correcto
    const jwtToken = user.generateJWT();
    res.status(200).send({
        _id: user._id,
        nombre: user.nombre,
        DNI   : user.DNI,
        email: user.email,
        token : jwtToken     
    })
})

router.put('/:id', [auth], [
    //Validación express-validator
    check('nombre').not().isEmpty().isLength({max: 50}),    
    check('apellido').not().isEmpty().isLength({max: 50}),
    check('email').not().isEmpty().isEmail().isLength({max: 70}),    
    check('clave').not().isEmpty().isLength({min: 6}).isLength({max: 15})   
], async(req,res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() });
    } 
    //Encripta la clave del usuario para guardarla
    const salt = await bcrypt.genSalt(10)
    const hashPassword = await bcrypt.hash(req.body.clave, salt)
    const session = await mongoose.startSession()//
    session.startTransaction()//
    try{
        const user = await User.findByIdAndUpdate(req.params.id,{
            nombre:req.body.nombre,
            apellido: req.body.apellido,
            email: req.body.email,
            clave: hashPassword
        },
        {
            new: true
        })
        if(!user){
            return res.status(404).send('El usuario con ese email no está.')
        }       
        res.status(204).send()
    }catch(e){
        await session.abortTransaction()
        session.endSession
        res.status(500).send(e.message)
    }
})

//Ejemplo de DELETE
router.delete('/:id', [auth], async(req,res)=>{
    const session = await mongoose.startSession()//
    session.startTransaction()//
    try{
        const user = await User.findOne({_id: req.params.id})
        if(!user){
            return res.status(404).send('El usuario con ese ID no está, no se puede eliminar.')
        }else{
            const user = await User.findByIdAndDelete(req.params.id)
        }
     
        res.status(200).send('Usuario eliminado')
    }catch(e){
        await session.abortTransaction()
        session.endSession
        res.status(500).send(e.message)
    }
})

module.exports = router
