import { LitElement, html } from 'lit-element';

export class Cotizacion extends LitElement {
    static get properties() {
        return {
            resultado: { type: String },
        };
    }
    constructor(){
        super();
        this.resultado = "";
    }
    render() {
        return html`
        <style>
            #cotizacion {
                background: #144575;                
                width: 100%;                
                height: 60px;
                
                display: flex;
           
            }
            button {
                border-radius: 6px;
                border: 2px solid #34a042;
                width: 180px;
                height: 40px;
                color: #34a042;
                background: #144575; 
                            
            }
            button:hover {
                color: #ffffff;
                background: #34a042;
            }
            p {
                color: #34a042;
                font-size: 1em;
                width: 100%;
                height: auto;
                
            }
            @media screen and (max-width: 800px) {
                #cotizacion {
                    display: flex;
                    flex-direction:column;
                }
                button {
                border-radius: 6px;
                border: 2px solid #34a042;
                width: 150px;
                height: 40px;
                color: #34a042;
                background: #144575;               
            }
                p {
                    color: #34a042;
                    font-size: 0.7em;
                    width: 100%;
                    height: auto;
                    margin: 5px;
                }            
            }

        </style>
        <div id="cotizacion">
            <p>${this.resultado}</p>
            <button @click="${this.cotizacion}">Obtener Cotización</button><br>
            
        </div>   
    `;
    }

    cotizacion(){
        const resultado = document.querySelector("#resultado")
    

        fetch("https://www.dolarsi.com/api/api.php?type=valoresprincipales")
            .then(res => res.json())
            .then(data => {
                const tipoCotizacion = data[0].casa.nombre;
                const compra         = data[0].casa.compra;
                const venta          = data[0].casa.venta;
    
                this.resultado = "Cotización " + tipoCotizacion + " - Compra: " + compra.replace(/,/g, '.') + " - Venta: " + venta.replace(/,/g, '.');

        });
    } 
}
  
customElements.define('cotizacion-element', Cotizacion);