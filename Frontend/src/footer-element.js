import { LitElement, html } from 'lit-element';

export class FooterElement extends LitElement {
    render() {
        return html`
            <style>                    
                
                footer {
                    background: #144575;
                    width: 90%;
                    margin-right: auto;
                    margin-left: auto;
                    padding:20px;
                    /* Flex */
                    display: flex;
                    flex-wrap: wrap;
                    justify-content: space-between;
                }

                footer .links {
                    background: #144575;
                    display:flex;
                    flex-wrap:wrap;
                }

                footer .links a {
                    flex-grow: 1;
                    color: #34a042;
                    padding: 10px;
                    text-align: center;
                    text-decoration: none;
                }

                footer .links a:hover {
                    background:  #1d6d28;;
                    color: #4bca5c;
                }

                footer .social {
                    background:   #144575;                     
                }

                footer .social a {
                    color: #144575;
                    background:   #34a042;
                    text-decoration: none;
                    padding: 10px;
                    display: inline-block;

                    -moz-border-radius: 50%;
                    -webkit-border-radius: 50%;
                    border-radius: 50%;
                }
            </style>
            <footer>
                <section class="links">
                    <a href="index.html">Inicio</a>
                    <a href="preguntas.html">Preguntas Frecuentes</a>
                    <a href="login.html">Iniciar Sesión</a>
                    <a href="AltaUsuario">Registrarme</a>                            
                </section>
                <div class="social">
                    <a href="https://www.facebook.com/silvia.sola.75 " target="_blank">FB</a>
                    <a href="#">IN</a>
                </div>
            </footer>        
        `;
    }
}
customElements.define('footer-element', FooterElement);