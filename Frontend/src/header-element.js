import { LitElement, html } from 'lit-element';

export class HeaderElement extends LitElement {
    render() {
        return html`
            <style>
                header {
                    background: #144575;
                    width:150%;
                    padding:20px;
                    margin: 0 auto;

                    /*Flex*/
                    display: flex;
                    justify-content: space-between;
                    align-items: center;
                    flex-direction: row;
                    flex-wrap: wrap;
                    
                }

                header .logo {
                    color:#fff;
                    font-size:30px;
                }

                header .logo img {
                    width: 50px;
                    vertical-align: top;
                }

                header .logo a {
                    color: #34a042;
                    text-decoration: none;
                    font-family: 'Courgette', cursive;
                    
                    line-height: 50px;
                }

                header nav {
                    width: 50%;
                    /* Flex */
                    display: flex;
                    flex-wrap:wrap;
                    align-items: center;
                }

                header nav a {
                    background: #144575;
                    color:#34a042;
                    text-align: center;
                    text-decoration: none;
                    padding: 5px;
                    display: inline-block;
                    /* Flex */
                    flex-grow: 1;
                }

                header nav a::hover {
                    background: #144575;
                    color:#34a042;
                    text-align: center;
                    text-decoration: none;
                    padding: 5px;
                    display: inline-block;
                    /* Flex */
                    flex-grow: 1;
                }
            </style>
            <header>
                <div class="logo">
                    <img src="images/logo.jpg" width="150" alt="">
                    <a href="#">Casa Dolores</a>
                </div>
                <nav>
                    <a href="index.html">Inicio</a>
                    <a href="preguntas.html">Preguntas Frecuentes</a>
                    <a href="login.html">Iniciar Sesión</a>
                    <a href="AltaUsuario.html">Registrarme</a>                
                </nav>
            </header>        
        `;
    }
}
customElements.define('header-element', HeaderElement);